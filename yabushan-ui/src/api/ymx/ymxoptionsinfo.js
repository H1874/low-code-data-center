import request from '@/utils/request'

// 查询个人信息选项列表
export function listYmxoptionsinfo(query) {
  return request({
    url: '/ymx/ymxoptionsinfo/list',
    method: 'get',
    params: query
  })
}

// 查询个人信息选项详细
export function getYmxoptionsinfo(optionsId) {
  return request({
    url: '/ymx/ymxoptionsinfo/' + optionsId,
    method: 'get'
  })
}

// 新增个人信息选项
export function addYmxoptionsinfo(data) {
  return request({
    url: '/ymx/ymxoptionsinfo',
    method: 'post',
    data: data
  })
}

// 修改个人信息选项
export function updateYmxoptionsinfo(data) {
  return request({
    url: '/ymx/ymxoptionsinfo',
    method: 'put',
    data: data
  })
}

// 删除个人信息选项
export function delYmxoptionsinfo(optionsId) {
  return request({
    url: '/ymx/ymxoptionsinfo/' + optionsId,
    method: 'delete'
  })
}

// 导出个人信息选项
export function exportYmxoptionsinfo(query) {
  return request({
    url: '/ymx/ymxoptionsinfo/export',
    method: 'get',
    params: query
  })
}