import request from '@/utils/request'
import CryptoJS from 'crypto-js/crypto-js'
// 默认的 KEY 与 iv 如果没有给
const KEY = CryptoJS.enc.Utf8.parse("1234567890123456");
const IV = CryptoJS.enc.Utf8.parse('1234567890123456');

// 查询自定义表单列表
export function listInfos(query) {
  return request({
    url: '/form/infos/list',
    method: 'get',
    params: query
  })
}

// 查询自定义表单详细
export function getInfos(id) {
  return request({
    url: '/form/infos/' + id,
    method: 'get'
  })
}

// 新增自定义表单
export function addInfos(data) {
  return request({
    url: '/form/infos',
    method: 'post',
    data: data
  })
}

// 修改自定义表单
export function updateInfos(data) {
  return request({
    url: '/form/infos',
    method: 'put',
    data: data
  })
}

// 删除自定义表单
export function delInfos(id) {
  return request({
    url: '/form/infos/' + id,
    method: 'delete'
  })
}



// 跳转编辑页面操作
export function formPage() {
  return request({
    url: '/form/infos/redirect',
    method: 'get'
  })
}

// 导出自定义表单
export function exportInfos(query) {
  return request({
    url: '/form/infos/export',
    method: 'get',
    params: query
  })
}

//生成数据库表
export function createTable(id) {
  return request({
    url: '/form/infos/createTable/' + id,
    method: 'post'
  })
}



/**
 * AES加密 ：字符串 key iv  返回base64 
 */
export function Encrypt(word, keyStr, ivStr) {
  let key = KEY
  let iv = IV

  if (keyStr) {
    key = CryptoJS.enc.Utf8.parse(keyStr);
    iv = CryptoJS.enc.Utf8.parse(ivStr);
  }

  let srcs = CryptoJS.enc.Utf8.parse(word);
  var encrypted = CryptoJS.AES.encrypt(srcs, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.ZeroPadding
  });
 // console.log("-=-=-=-", encrypted.ciphertext)
  return CryptoJS.enc.Base64.stringify(encrypted.ciphertext);

}
/**
 * AES 解密 ：字符串 key iv  返回base64 
 *
 */
export function Decrypt(word, keyStr, ivStr) {
  let key  = KEY
  let iv = IV

  if (keyStr) {
    key = CryptoJS.enc.Utf8.parse(keyStr);
    iv = CryptoJS.enc.Utf8.parse(ivStr);
  }

  let base64 = CryptoJS.enc.Base64.parse(word);
  let src = CryptoJS.enc.Base64.stringify(base64);

  var decrypt = CryptoJS.AES.decrypt(src, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.ZeroPadding
  });

  var decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
  return decryptedStr.toString();
}