package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.DataAliApiLog;
import com.yabushan.system.service.IDataAliApiLogService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 聚合日志Controller
 *
 * @author yabushan
 * @date 2021-08-08
 */
@RestController
@RequestMapping("/system/aliLog")
public class DataAliApiLogController extends BaseController
{
    @Autowired
    private IDataAliApiLogService dataAliApiLogService;

    /**
     * 查询聚合日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:aliLog:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataAliApiLog dataAliApiLog)
    {
        startPage();
        List<DataAliApiLog> list = dataAliApiLogService.selectDataAliApiLogList(dataAliApiLog);
        return getDataTable(list);
    }

    /**
     * 导出聚合日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:aliLog:export')")
    @Log(title = "聚合日志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DataAliApiLog dataAliApiLog)
    {
        List<DataAliApiLog> list = dataAliApiLogService.selectDataAliApiLogList(dataAliApiLog);
        ExcelUtil<DataAliApiLog> util = new ExcelUtil<DataAliApiLog>(DataAliApiLog.class);
        return util.exportExcel(list, "aliLog");
    }

    /**
     * 获取聚合日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:aliLog:query')")
    @GetMapping(value = "/{logId}")
    public AjaxResult getInfo(@PathVariable("logId") Long logId)
    {
        return AjaxResult.success(dataAliApiLogService.selectDataAliApiLogById(logId));
    }

    /**
     * 新增聚合日志
     */
    @PreAuthorize("@ss.hasPermi('system:aliLog:add')")
    @Log(title = "聚合日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataAliApiLog dataAliApiLog)
    {
        return toAjax(dataAliApiLogService.insertDataAliApiLog(dataAliApiLog));
    }

    /**
     * 修改聚合日志
     */
    @PreAuthorize("@ss.hasPermi('system:aliLog:edit')")
    @Log(title = "聚合日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataAliApiLog dataAliApiLog)
    {
        return toAjax(dataAliApiLogService.updateDataAliApiLog(dataAliApiLog));
    }

    /**
     * 删除聚合日志
     */
    @PreAuthorize("@ss.hasPermi('system:aliLog:remove')")
    @Log(title = "聚合日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{logIds}")
    public AjaxResult remove(@PathVariable Long[] logIds)
    {
        return toAjax(dataAliApiLogService.deleteDataAliApiLogByIds(logIds));
    }
}
