package com.yabushan.form.service.impl;

import java.util.List;
import com.yabushan.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yabushan.form.mapper.JimuReportDbFieldMapper;
import com.yabushan.form.domain.JimuReportDbField;
import com.yabushan.form.service.IJimuReportDbFieldService;

/**
 * 报表Service业务层处理
 *
 * @author yabushan
 * @date 2021-07-03
 */
@Service("yabushandbfield")
public class JimuReportDbFieldServiceImpl implements IJimuReportDbFieldService
{
    @Autowired
    private JimuReportDbFieldMapper jimuReportDbFieldMapper;

    /**
     * 查询报表
     *
     * @param id 报表ID
     * @return 报表
     */
    @Override
    public JimuReportDbField selectJimuReportDbFieldById(String id)
    {
        return jimuReportDbFieldMapper.selectJimuReportDbFieldById(id);
    }

    /**
     * 查询报表列表
     *
     * @param jimuReportDbField 报表
     * @return 报表
     */
    @Override
    public List<JimuReportDbField> selectJimuReportDbFieldList(JimuReportDbField jimuReportDbField)
    {
        return jimuReportDbFieldMapper.selectJimuReportDbFieldList(jimuReportDbField);
    }

    /**
     * 新增报表
     *
     * @param jimuReportDbField 报表
     * @return 结果
     */
    @Override
    public int insertJimuReportDbField(JimuReportDbField jimuReportDbField)
    {
        jimuReportDbField.setCreateTime(DateUtils.getNowDate());
        return jimuReportDbFieldMapper.insertJimuReportDbField(jimuReportDbField);
    }

    /**
     * 修改报表
     *
     * @param jimuReportDbField 报表
     * @return 结果
     */
    @Override
    public int updateJimuReportDbField(JimuReportDbField jimuReportDbField)
    {
        jimuReportDbField.setUpdateTime(DateUtils.getNowDate());
        return jimuReportDbFieldMapper.updateJimuReportDbField(jimuReportDbField);
    }

    /**
     * 批量删除报表
     *
     * @param ids 需要删除的报表ID
     * @return 结果
     */
    @Override
    public int deleteJimuReportDbFieldByIds(String[] ids)
    {
        return jimuReportDbFieldMapper.deleteJimuReportDbFieldByIds(ids);
    }

    /**
     * 删除报表信息
     *
     * @param id 报表ID
     * @return 结果
     */
    @Override
    public int deleteJimuReportDbFieldById(String id)
    {
        return jimuReportDbFieldMapper.deleteJimuReportDbFieldById(id);
    }
}
