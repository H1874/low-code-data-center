package com.yabushan.system.mapper;

import java.util.List;
import com.yabushan.system.domain.UumUserinfo;

/**
 * 租户用户Mapper接口
 *
 * @author yabushan
 * @date 2021-04-18
 */
public interface UumUserinfoMapper
{
    /**
     * 查询租户用户
     *
     * @param employeeId 租户用户ID
     * @return 租户用户
     */
    public UumUserinfo selectUumUserinfoById(String employeeId);

    /**
     * 查询租户用户
     *
     * @param loginId 用户账号
     * @return 租户用户
     */
    public UumUserinfo selectUumUserinfoByLoginId(String loginId);
    /**
     * 查询租户用户列表
     *
     * @param uumUserinfo 租户用户
     * @return 租户用户集合
     */
    public List<UumUserinfo> selectUumUserinfoList(UumUserinfo uumUserinfo);

    /**
     * 新增租户用户
     *
     * @param uumUserinfo 租户用户
     * @return 结果
     */
    public int insertUumUserinfo(UumUserinfo uumUserinfo);

    /**
     * 修改租户用户
     *
     * @param uumUserinfo 租户用户
     * @return 结果
     */
    public int updateUumUserinfo(UumUserinfo uumUserinfo);

    /**
     * 删除租户用户
     *
     * @param employeeId 租户用户ID
     * @return 结果
     */
    public int deleteUumUserinfoById(String employeeId);

    /**
     * 批量删除租户用户
     *
     * @param employeeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteUumUserinfoByIds(String[] employeeIds);
}
